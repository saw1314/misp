#include <iostream>
#include <fstream>
#include <string>
#include <algorithm>
#include <iterator>
#include <cstddef> // для ptrdiff_t

using namespace std;

class example
{
public:
	int a;
	int b;
	int c;

	example()
	{
		a = b = c = 0;
	}
	example(int a, int b, int c)
	{
		this->a = a;
		this->b = b;
		this->c = c;
	}

	void PrintInfo()
	{
		cout << "a = " << this->a << "\n" << "b = " << this->b << "\n" << "c = " << this->c << "\n";
	}

	friend ostream& operator<<(ostream& os, const example& ex);

};

ostream& operator<<(ostream& os, const example& ex)
{
	os << ex.a << " " << ex.b << " " << ex.c<<endl;
	
	return os;
}


template<typename T>
class FileArrProxy
{
	fstream* file;

	int index;

public:

	FileArrProxy(fstream* file,int index):file{file},index{index}{}

	void operator=(const T temp)
	{
		T buff;

		file->seekp(sizeof(T)*index,ios_base::beg);

		file->write((char*)&temp,sizeof(T));
	}

	operator const T()const
	{
		T buff;
		
		file->seekg(sizeof(T)*index,ios_base::beg);

		file->read((char*)&buff, sizeof(T));

		return buff;
	}

};

template<typename T> class FileArr;
template<typename T> ostream& operator<<(ostream&,FileArr<T>&);

template<typename T>
class FileArr
{
	fstream file;

public:

	friend class Iterator;

	explicit FileArr(const string& path)
	{
		file.open(path, ios_base::in | ios_base::app | ios_base::binary);

		if (!file.is_open())
		{
			cout << "Ошибка открытия файла!" << "\n";
			system("pause");
		}
	}

	~FileArr() { file.close();file.clear();}

	void write_back(T obj){file.write((char*)&obj, sizeof(T));}
	
	void read_all()
	{
		file.seekg(0,ios_base::beg);

		T buff;

		int size = file_size();

		for(int i=0 ;i<size; ++i)
		{
			file.read((char*)&buff,sizeof(T));

			cout<<buff;
		}

		file.seekg(ios_base::beg);
	}
	
	void read(int index)
	{
		T buff;

		file.seekg(sizeof(T)*index,ios_base::beg);

		file.read((char*)&buff, sizeof(T));

		cout<<buff;
	}
	
	int file_size()
	{
		file.seekg(0,ios_base::end);

		int i=file.tellg();

		file.seekg(ios_base::beg);

		return i/sizeof(T);
	}

	FileArrProxy<T> operator[](int index)
	{
		file.seekg(ios_base::beg);

		file.seekp(ios_base::beg);

		return FileArrProxy<T>(&file,index);
	}

	struct Iterator
	{
		using iterator_category = forward_iterator_tag;
		using difference_type = ptrdiff_t;
		using pointer = fstream*;  
		using type = T;  
		// теги для algorithm
	private:
		pointer m_ptr;
		int file_size;
		bool flag;
	public:
		
		Iterator(fstream* ptr,bool flag,int file_size_) :m_ptr(ptr),file_size{file_size_},flag{flag}{}

	    type operator*() 
	    { 
	     	T buff;

	     	if(flag)
	     	{
	    		m_ptr->read((char*)&buff,sizeof(T)); 

	    		m_ptr->seekg(m_ptr->tellg()-sizeof(T),ios_base::beg);
	    	}

	    	else 
	    	{
	    		ios::pos_type current = m_ptr->tellg();
	    		
	    		m_ptr->seekg(file_size*sizeof(T)-sizeof(T),ios_base::beg);

	    		m_ptr->read((char*)&buff,sizeof(T)); 

	    		m_ptr->seekg(current,ios_base::beg);
	    	}

	    	return buff; 
	    }

        Iterator& operator++() 
        {
        	if(flag)
        		m_ptr->seekg(sizeof(T),ios_base::cur);

        	return *this;
        }

        bool operator==(Iterator& a)
		{
			ios::pos_type a_pos = a.m_ptr->tellg();

			m_ptr->seekg(file_size*sizeof(T),ios_base::beg);

			ios::pos_type m_pos = m_ptr->tellg();

			m_ptr->seekg(a_pos,ios_base::beg);

			return a_pos==m_pos;
		}

        bool operator!=(Iterator& a)
		{
			ios::pos_type a_pos = a.m_ptr->tellg();

			m_ptr->seekg(file_size*sizeof(T),ios_base::beg);

			ios::pos_type m_pos = m_ptr->tellg();

			m_ptr->seekg(a_pos,ios_base::beg);

			return a_pos!=m_pos;
		}
	};

	Iterator begin() 
	{
		int file_size_ = file_size();

		file.seekg(0);

		return Iterator(&file,true,file_size_);
	} 

	Iterator end()
	{
		int file_size_ = file_size();

		file.seekg(0);
		
		return Iterator(&file,false,file_size_);
	}

	friend ostream& operator<<<>(ostream&,FileArr&);
};

template<typename T>
ostream& operator<<(ostream& out,FileArr<T>& temp)
{
	for(int i=0 ; i<temp.file_size(); ++i)
		cout<<temp[i];

	return out<<endl;
}

int main()
{
	setlocale(LC_ALL, "Russian");
	//SetConsoleCP(1251);
	example a(1, 2, 3);
	example b(4,5,6);
	example c(7, 8, 9);

	const string path = "t.bin";

	FileArr<example> exArr(path);

	exArr.write_back(a);
	exArr.write_back(b);
	exArr.write_back(c);

	exArr.read(2);
	exArr.read(1);
	cout<<endl;
	exArr.read_all();
	cout<<endl;
	exArr.read(0);
	exArr[0] = c;
	exArr.read(0);
	cout<<endl;
	cout<<exArr;

	for(auto i = exArr.begin(),j = exArr.end();i!=j;++i )
		cout<<*i<<endl;

	return 0;
}


